package be.bstorm.spring.orsys2023graphql.models;

import jakarta.persistence.*;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Entity
@Data
public class Author {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String name;
    private String thumbnail;

    @ManyToMany(mappedBy = "authors")
    private List<Post> posts = new ArrayList<>();
}
