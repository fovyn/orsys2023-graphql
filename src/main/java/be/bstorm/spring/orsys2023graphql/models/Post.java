package be.bstorm.spring.orsys2023graphql.models;

import jakarta.persistence.*;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Entity
@Data
public class Post {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String title;
    @Column(nullable = false)
    private String text;
    private String category;


    @ManyToMany
    private List<Author> authors = new ArrayList<>();
}
