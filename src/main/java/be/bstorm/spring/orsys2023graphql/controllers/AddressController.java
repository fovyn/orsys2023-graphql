package be.bstorm.spring.orsys2023graphql.controllers;

import be.bstorm.spring.orsys2023graphql.forms.FAddressCreate;
import be.bstorm.spring.orsys2023graphql.models.Address;
import be.bstorm.spring.orsys2023graphql.repositories.AddressRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.stereotype.Controller;

@Controller
@RequiredArgsConstructor
public class AddressController {
    private final AddressRepository addressRepository;
    @MutationMapping
    public Address createAddress(@Argument FAddressCreate form) {
        Address address = form.toBll();

        return addressRepository.save(address);
    }
}
