package be.bstorm.spring.orsys2023graphql.controllers;

import be.bstorm.spring.orsys2023graphql.forms.FPostCreate;
import be.bstorm.spring.orsys2023graphql.models.Author;
import be.bstorm.spring.orsys2023graphql.models.Post;
import be.bstorm.spring.orsys2023graphql.repositories.AuthorRepository;
import be.bstorm.spring.orsys2023graphql.repositories.PostRepository;
import jakarta.persistence.criteria.CriteriaQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;
import org.springframework.graphql.data.method.annotation.SchemaMapping;
import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class PostController {
    private final PostRepository postRepository;
    private final AuthorRepository authorRepository;

    @Autowired
    public PostController(PostRepository postRepository, AuthorRepository authorRepository) {
        this.postRepository = postRepository;
        this.authorRepository = authorRepository;
    }

    @QueryMapping
    public List<Post> posts(
            @Argument int page,
            @Argument int size
    ) {
        return this.postRepository.findAll(PageRequest.of(page, size)).stream().toList();
    }

    @QueryMapping
    public Post post(@Argument long postId) {
        return this.postRepository.findById(postId).orElseThrow(() -> new RuntimeException(""));
    }

    @QueryMapping
    public List<Post> postsBy(@Argument String name) {
        return this.postRepository.findAllByAuthorName(name);
    }

    @MutationMapping
    public Post createPost(@Argument FPostCreate post) {
        Post toCreate = post.toBll();
        toCreate.getAuthors().addAll(this.authorRepository.findAllById(post.authorIds()));

        return this.postRepository.save(toCreate);
    }


    @SchemaMapping
    public List<Author> authors(Post post, @Argument String name) {
        Specification<Author> specification = (author, cq, cb) -> cb.isMember(post, author.get("posts"));

        if (name != null) {
            specification.and((author, cq, cb) -> cb.like(author.get("name"), "%"+ name+ "%"));
        }

        return authorRepository.findAll(specification);
    }
}
