package be.bstorm.spring.orsys2023graphql.controllers;

import be.bstorm.spring.orsys2023graphql.models.Author;
import be.bstorm.spring.orsys2023graphql.repositories.AuthorRepository;
import org.springframework.graphql.data.method.annotation.QueryMapping;
import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class AuthorController {
    private final AuthorRepository authorRepository;

    public AuthorController(AuthorRepository authorRepository) {
        this.authorRepository = authorRepository;
    }

    @QueryMapping
    public List<Author> authors() {
        return authorRepository.findAll();
    }
}
