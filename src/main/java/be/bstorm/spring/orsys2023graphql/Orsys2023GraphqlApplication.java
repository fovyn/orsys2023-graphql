package be.bstorm.spring.orsys2023graphql;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Orsys2023GraphqlApplication {

    public static void main(String[] args) {
        SpringApplication.run(Orsys2023GraphqlApplication.class, args);
    }

}
