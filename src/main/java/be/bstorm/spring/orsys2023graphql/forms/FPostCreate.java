package be.bstorm.spring.orsys2023graphql.forms;

import be.bstorm.spring.orsys2023graphql.models.Post;

import java.util.List;

public record FPostCreate(
        String title,
        String text,
        String category,
        List<Long> authorIds
) {

    public Post toBll() {
        Post post = new Post();

        post.setTitle(title());
        post.setText(text());
        post.setCategory(category());

        return post;
    }
}
