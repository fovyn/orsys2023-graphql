package be.bstorm.spring.orsys2023graphql.forms;

import be.bstorm.spring.orsys2023graphql.models.Address;

public record FAddressCreate(
        String city,
        String postalCode
) {

    public Address toBll() {
        Address address = new Address();

        address.setCity(city());
        address.setPostalCode(postalCode());

        return address;
    }
}
