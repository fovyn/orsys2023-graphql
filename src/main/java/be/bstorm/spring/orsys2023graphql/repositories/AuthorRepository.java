package be.bstorm.spring.orsys2023graphql.repositories;

import be.bstorm.spring.orsys2023graphql.models.Author;
import be.bstorm.spring.orsys2023graphql.models.Post;
import org.hibernate.query.criteria.JpaCriteriaQuery;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AuthorRepository extends JpaRepository<Author, Long>, JpaSpecificationExecutor<Author> {

    @Query(value = "SELECT a FROM Author a JOIN a.posts p WHERE p = :post")
    List<Author> findAllByPost(@Param("post") Post post);

    @Query(value = "SELECT a FROM Author a JOIN a.posts p WHERE p = :post AND a.name = :name")
    List<Author> findAllByPostAndName(
            @Param("post") Post post,
            @Param("name") String name
    );
}
