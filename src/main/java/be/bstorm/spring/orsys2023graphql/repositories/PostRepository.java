package be.bstorm.spring.orsys2023graphql.repositories;

import be.bstorm.spring.orsys2023graphql.models.Post;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PostRepository extends JpaRepository<Post, Long> {

    @Query("SELECT p FROM Post p JOIN p.authors a WHERE a.name = :authorName")
    List<Post> findAllByAuthorName(@Param("authorName") String authorName);
}
