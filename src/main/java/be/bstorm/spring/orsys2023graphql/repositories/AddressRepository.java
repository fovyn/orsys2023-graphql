package be.bstorm.spring.orsys2023graphql.repositories;

import be.bstorm.spring.orsys2023graphql.models.Address;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AddressRepository extends JpaRepository<Address, Long> {
}
