package be.bstorm.spring.orsys2023graphql.exceptions;

import graphql.GraphQLError;
import graphql.language.SourceLocation;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.graphql.execution.ErrorType;
import org.springframework.http.HttpStatus;

import java.util.List;

@Getter
@NoArgsConstructor
public class FieldRequiredException extends RuntimeException implements GraphQLError {
    private HttpStatus httpStatus = HttpStatus.PRECONDITION_FAILED;
    private String message = "Precondition failed";

    private List<SourceLocation> locations;

    public FieldRequiredException(String message, List<SourceLocation> locations) {
        this.message = message;
        this.locations = locations;
    }

    @Override
    public List<SourceLocation> getLocations() {
        return locations;
    }

    @Override
    public ErrorType getErrorType() {
        return ErrorType.BAD_REQUEST;
    }
}
